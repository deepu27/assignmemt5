const xhr=new XMLHttpRequest();
xhr.open('GET','https://restcountries.eu/rest/v2/all',true);
xhr.onload=function(){
        let obj=JSON.parse(this.responseText);
        console.log(obj);
        let list1=document.getElementById('list1');
        let list2=document.getElementById('list2');
        str1="";
        str2="";
        for(key in obj){
            if(obj[key].name==="Afghanistan"){
                str1+='<img src="' + obj[key].flag + '" width="100px" height="100px">';
                str2+='<li>Native Name:'+obj[key].nativeName+'</li>';
                str2+='<li>Capital: '+obj[key].capital+'</li>';
                str2+='<li>Population: '+obj[key].population+'</li>';
                str2+='<li>Region: '+obj[key].region+'</li>';
                str2+='<li>Sub-Region: '+obj[key].subregion+'</li>';
                str2+='<li>Area: '+obj[key].area+'</li>';
                str2+='<li>Country Code: '+obj[key].callingCodes+'</li>';
                str2+='<li> Languages: '+obj[key].languages.name+'</li>';
                str2+='<li>Currencies: '+obj[key].currencies.name+'</li>';
                str2+='<li>Time Zone: '+obj[key].timezones+'</li>';
                

            }
        }
        list1.innerHTML=str1;
        list2.innerHTML=str2;
}
xhr.send();